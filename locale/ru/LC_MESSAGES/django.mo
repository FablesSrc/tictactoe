��    #      4  /   L           	     )     :  J   <  G   �     �  
   �     �     �     �               "     2     >     E     X     ^     e     n     w     z     �     �     �     �  	   �  -   �  :   �     (     :     >     F     I  �  Z  4        E     a  `   c  l   �     1     @     W     u  .   �     �     �  
   �     �      	     	  
   -	  
   8	     C	     S	     g	     n	     �	     �	     �	  $   �	     �	  w   

  H   �
     �
     �
     �
                   	                    #                                                                  
   "       !                                                     You've lost, %(username)s won! %(username)s win - <span class="counter-value"></span> seconds left until end of his/her turn <span class="counter-value"></span> seconds left until end of your turn Accept Acceptance Active players Cancel Congratulations! You win! Decline I invite In a drawn game Invitations Invite Invite on Big game Login Logout My games New game No Nobody is there On main board Opponent turn Repeat Sign Up TicTacToe User %(username)s wants to try again. Accept? Wait for <span class="username">`%(user)s`</span> approval Wait for approval Yes You win vs your time is out Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-09 12:46+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Вы проиграли, выиграл %(username)s Выиграл %(username)s - Осталось <span class="counter-value"></span> секунд(ы) до его/её хода. Осталось <span class="counter-value"></span> секунд(ы) до конца вашего хода. Принять Приглашение Активные игроки Отменить Поздравляем! Вы выиграли! Отклонить Я пригласил Ничья Приглашения Пригласить Большая игра Войти Выйти Мои игры Новая игра Нет Никого нету На главную Ходит соперник Повторить Зарегистрироваться Крестики-Нолики Пользователь `%(username)s` предлагает повторить. Принять приглашение? Ожидаем согласия <span class="username">`%(user)s`</span> Ожидаем согласия Да Ты выиграл против ваше время вышло 