# coding=utf-8
import json
from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase

# Create your tests here.
from tictactoe.models import Board, Invitation, UserActivity


class TestBoardModel(TestCase):
    def setUp(self):
        self.board = Board.objects.create(width=3, height=3)
        self.bigboard = Board.objects.create(width=10, height=10, line_width=5)

    def test_should_return_correct_winner(self):
        self.board.state = json.dumps(
            [
                1, 1, 1,
                2, 0, 2,
                2, 0, 2
            ]
        )
        self.assertEqual(self.board.get_winner(), 1)

        self.board.state = json.dumps(
            [
                2, 1, 1,
                2, 0, 2,
                2, 0, 2
            ]
        )
        self.assertEqual(self.board.get_winner(), 2)

        self.board.state = json.dumps(
            [
                1, 3, 1,
                1, 3, 2,
                2, 3, 2
            ]
        )
        self.assertEqual(self.board.get_winner(), 3)

        self.board.state = json.dumps(
            [
                4, 3, 1,
                1, 4, 2,
                2, 3, 4
            ]
        )
        self.assertEqual(self.board.get_winner(), 4)

        self.board.state = json.dumps(
            [
                4, 3, 5,
                1, 5, 2,
                5, 3, 4
            ]
        )
        self.assertEqual(self.board.get_winner(), 5)

        self.board.state = json.dumps(
            [
                4, 3, 6,
                1, 5, 6,
                5, 3, 6
            ]
        )
        self.assertEqual(self.board.get_winner(), 6)

        self.board.state = json.dumps(
            [
                4, 3, 6,
                1, 5, 6,
                7, 7, 7
            ]
        )
        self.assertEqual(self.board.get_winner(), 7)

        self.bigboard.state = json.dumps(
            [
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 5, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 5, 0, 0, 0, 0, 0, 6,
                0, 0, 0, 0, 5, 0, 0, 0, 6, 0,
                0, 0, 0, 0, 0, 5, 0, 6, 0, 0,
                0, 0, 0, 0, 0, 0, 6, 0, 0, 0,
                0, 0, 0, 0, 0, 6, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            ]
        )
        self.assertEqual(self.bigboard.get_winner(), 6)


class TestViewBase(TestCase):
    def setUp(self):
        settings.LONG_POLLING_TIMEOUT = 2
        self.user1 = User.objects.create_user(username="user1", password="123")
        self.user2 = User.objects.create_user(username="user2", password="123")

    @staticmethod
    def login_as_user1(fn):
        def __wrapper(self=None, *args, **kwargs):
            self.client.login(username=self.user1, password="123")
            fn(self)
            self.client.logout()

        return __wrapper

    @staticmethod
    def login_as_user2(fn):
        def __wrapper(self=None, *args, **kwargs):
            self.client.login(username=self.user2, password="123")
            fn(self)
            self.client.logout()

        return __wrapper


# ЭТИ ТЕСТЫ ДОБАВЛЕНЫ ПОСЛЕ СДАЧИ РАБОТЫ
class TestBoardView(TestViewBase):
    def test_guest_CANT_see_board(self):
        board = Board.objects.create()
        response = self.client.get(reverse("board", kwargs={"pk": board.pk}))
        self.assertEqual(302, response.status_code)

    @TestViewBase.login_as_user1
    def test_user_CAN_see_board(self):
        board = Board.objects.create()
        response = self.client.get(reverse("board", kwargs={"pk": board.pk}))
        self.assertEqual(200, response.status_code)


class TestIndexView(TestViewBase):
    def test_guest_CANT_see_index_page(self):
        response = self.client.get(reverse("index"))
        self.assertEqual(302, response.status_code)

    @TestViewBase.login_as_user1
    def test_user_CAN_see_index_page(self):
        response = self.client.get(reverse("index"))
        self.assertEqual(200, response.status_code)


class TestActiveUsersView(TestViewBase):
    def test_guest_CANT_get_active_users(self):
        response = self.client.get(reverse("active-users"))
        self.assertEqual(302, response.status_code)

    @TestViewBase.login_as_user1
    def test_user_CAN_get_active_users(self):
        response = self.client.get(reverse("active-users"))
        self.assertEqual(200, response.status_code)

    @TestViewBase.login_as_user1
    def test_user_CAN_get_active_users_with_hash_ajax(self):
        response = self.client.get(reverse("active-users"), {
            "hash": ""
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(200, response.status_code)

        data = json.loads(response.content)

        self.assertIsNotNone(data['html'])
        self.assertIsNotNone(data['hash'])

    def test_user_receive_200_if_provide_right_hash_ajax(self):
        self.login_as_user1(lambda self: self.client.get(reverse("im-here")))(self)
        self.login_as_user2(lambda self: self.client.get(reverse("im-here")))(self)

        def wrapper(self):
            response = self.client.get(reverse("active-users"), {
                "hash": ""
            }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
            self.assertEqual(200, response.status_code)

            data = json.loads(response.content)

            self.assertIsNotNone(data['html'])
            self.assertIsNotNone(data['hash'])

            response = self.client.get(reverse("active-users"), {
                "hash": data['hash']
            }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
            self.assertEqual(200, response.status_code)

        TestViewBase.login_as_user1(wrapper)(self)


class TestImHereView(TestViewBase):
    def test_guest_CANT_inform_about_his_activity(self):
        response = self.client.get(reverse("im-here"))
        self.assertEqual(302, response.status_code)

    @TestViewBase.login_as_user1
    def test_user_CAN_inform_about_his_activity(self):
        self.assertEqual(0, UserActivity.objects.filter(user=self.user1).count())
        response = self.client.get(reverse("im-here"))
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, UserActivity.objects.filter(user=self.user1).count())


class TestIsMyStepView(TestViewBase):
    def test_guest_CANT_check_his_step(self):
        board = Board.objects.create(user1=self.user1, user2=self.user2, current_user=self.user1)
        response = self.client.get(reverse("is-my-step", kwargs={"pk": board.pk}))
        self.assertEqual(response.status_code, 302)

    def test_user_CAN_check_his_step(self):
        board = Board.objects.create(user1=self.user1, user2=self.user2, current_user=self.user1)
        self.client.login(username=self.user1.username, password="123")
        response = self.client.get(reverse("is-my-step", kwargs={"pk": board.pk}))
        self.assertEqual(response.status_code, 200)
        self.client.logout()

        board = Board.objects.create(user1=self.user1, user2=self.user2, current_user=self.user1)
        self.client.login(username=self.user2.username, password="123")
        response = self.client.get(reverse("is-my-step", kwargs={"pk": board.pk}))
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    @TestViewBase.login_as_user1
    def test_user_check_with_hash(self):
        board = Board.objects.create(user1=self.user1, user2=self.user2, current_user=self.user1)
        response = self.client.get(reverse("is-my-step", kwargs={"pk": board.pk}), {
            "hash": ""
        })
        self.assertEqual(response.status_code, 200)

    @TestViewBase.login_as_user1
    def test_user_check_with_hash_ajax(self):
        board = Board.objects.create(user1=self.user1, user2=self.user2, current_user=self.user1)
        response = self.client.get(reverse("is-my-step", kwargs={"pk": board.pk}), {
            "hash": ""
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertTrue(data['my_step'])
        self.assertIn("winner", data)
        self.assertIn("hash", data)
        self.assertIn("html", data)


class TestListInvitations(TestViewBase):
    def setUp(self):
        super(TestListInvitations, self).setUp()

        self.invitation1 = Invitation.objects.create(
            from_user=self.user2,
            to_user=self.user1
        )
        self.invitation2 = Invitation.objects.create(
            from_user=self.user2,
            to_user=self.user1
        )

        self.invitation3 = Invitation.objects.create(
            from_user=self.user1,
            to_user=self.user2
        )

    def test_guest_CANT_list_his_invitations(self):
        response = self.client.get(reverse("invitations"))
        self.assertEqual(response.status_code, 302)

    @TestViewBase.login_as_user1
    def test_user_CAN_list_his_invitations(self):
        response = self.client.get(reverse("invitations"))
        self.assertEqual(response.status_code, 200)

    @TestViewBase.login_as_user1
    def test_user_list_invitations_with_hash(self):
        response = self.client.get(reverse("invitations"), {
            "hash": ""
        })
        self.assertEqual(response.status_code, 200)

    @TestViewBase.login_as_user1
    def test_user_list_invitations_with_hash__ajax(self):
        response = self.client.get(reverse("invitations"), {
            "hash": ""
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertIn("html", data)
        self.assertIn("hash", data)

    @TestViewBase.login_as_user1
    def test_user_list_invitations_returns_200_if_right_hash_provided_ajax(self):
        response = self.client.get(reverse("invitations"), {
            "hash": ""
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertIn("html", data)
        self.assertIn("hash", data)

        response = self.client.get(reverse("invitations"), {
            "hash": data['hash']
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)




class TestForRepeatInvitationView(TestViewBase):
    def test_guest_CANT_for_repeat_invitation(self):
        invitation1 = Invitation.objects.create(
            from_user=self.user2,
            to_user=self.user1
        )

        response = self.client.get(
            reverse("has-invitation-from", kwargs={"pk": invitation1.from_user.pk}),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 302)

    @TestViewBase.login_as_user1
    def test_check_for_repeat_invitation(self):
        invitation1 = Invitation.objects.create(
            from_user=self.user2,
            to_user=self.user1
        )

        response = self.client.get(
            reverse("has-invitation-from", kwargs={"pk": invitation1.from_user.pk}),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['decline'],
                         reverse("decline-invitation", kwargs={"pk": invitation1.pk}))
        self.assertEqual(data['accept'],
                         reverse("accept-invitation", kwargs={"pk": invitation1.pk}))


class TestInvitationsView(TestViewBase):
    def setUp(self):
        super(TestInvitationsView, self).setUp()
        self.user3 = User.objects.create_user("user3", password="123")

    def test_guest_cant_invite_user(self):
        response = self.client.get(
            reverse("invite-user", kwargs={"pk": self.user1.pk}))
        self.assertEqual(302, response.status_code)

    @TestViewBase.login_as_user1
    def test_user_cant_invite_twice(self):
        response = self.client.get(
            reverse("invite-user", kwargs={"pk": self.user2.pk}), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(200, response.status_code)

        response = self.client.get(
            reverse("invite-user", kwargs={"pk": self.user3.pk}), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(400, response.status_code)

    @TestViewBase.login_as_user2
    def test_user_can_accept_invitation_ajax(self):
        invitation = Invitation.objects.create(
            from_user=self.user1,
            to_user=self.user2
        )
        response = self.client.get(
            reverse("accept-invitation", kwargs={"pk": invitation.pk}),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)

        invitation.refresh_from_db()
        self.assertEqual(data['board'], invitation.board_id)

    @TestViewBase.login_as_user1
    def test_user_cant_invite_himself(self):
        response = self.client.get(
            reverse("invite-user", kwargs={"pk": self.user1.pk}))
        self.assertEqual(400, response.status_code)

    def test_that_we_can_invite_on_board_of_specific_size(self):
        self.client.login(username="user1", password="123")
        response = self.client.get(
            reverse("invite-user", kwargs={"pk": self.user2.pk}), {
                "width": 7,
                "height": 6,
                "line_width": 3,
            }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.client.logout()

        invitation = Invitation.objects.get(pk=data["invitation"])

        self.client.login(username="user2", password="123")
        response = self.client.get(
            reverse("accept-invitation", kwargs={"pk": invitation.pk}))
        self.assertEqual(response.status_code, 302)
        invitation.refresh_from_db()

        self.assertRedirects(response, reverse("board", kwargs={"pk": invitation.board.pk}))

        self.assertEqual(7, invitation.board.width)
        self.assertEqual(6, invitation.board.height)
        self.assertEqual(3, invitation.board.line_width)

        self.client.logout()

    def test_that_we_can_create_board(self):
        self.client.login(username="user1", password="123")
        response = self.client.get(
            reverse("invite-user", kwargs={"pk": self.user2.pk}), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.client.logout()

        invitation = Invitation.objects.get(pk=data["invitation"])

        self.client.login(username="user2", password="123")
        response = self.client.get(
            reverse("accept-invitation", kwargs={"pk": invitation.pk}))
        self.assertEqual(response.status_code, 302)
        invitation.refresh_from_db()

        self.assertRedirects(response, reverse("board", kwargs={"pk": invitation.board.pk}))

        self.assertEqual(3, invitation.board.width)
        self.assertEqual(3, invitation.board.height)
        self.assertEqual(3, invitation.board.line_width)

        self.client.logout()

    def test_that_we_can_create_big_board(self):
        self.client.login(username="user1", password="123")
        response = self.client.get(
            reverse("invite-user", kwargs={"pk": self.user2.pk}),
            {
                "big": ""
            }, HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.client.logout()

        invitation = Invitation.objects.get(pk=data["invitation"])

        self.client.login(username="user2", password="123")
        response = self.client.get(
            reverse("accept-invitation", kwargs={"pk": invitation.pk}))
        self.assertEqual(response.status_code, 302)
        invitation.refresh_from_db()

        self.assertRedirects(response, reverse("board", kwargs={"pk": invitation.board.pk}))

        self.assertEqual(10, invitation.board.width)
        self.assertEqual(10, invitation.board.height)
        self.assertEqual(5, invitation.board.line_width)

        self.client.logout()


class TestDeclineInvitationView(TestViewBase):
    def setUp(self):
        super(TestDeclineInvitationView, self).setUp()
        self.invitation = Invitation.objects.create(
            from_user=self.user2,
            to_user=self.user1
        )

    def test_guest_CANT_decline_invitation(self):
        response = self.client.get(reverse("decline-invitation",
                                           kwargs={"pk": self.invitation.pk}))
        self.assertEqual(302, response.status_code)

    @TestViewBase.login_as_user1
    def test_user_CAN_decline_invitation_and_that_will_remove_it(self):
        response = self.client.get(reverse("decline-invitation",
                                           kwargs={"pk": self.invitation.pk}))
        self.assertEqual(302, response.status_code)
        self.assertEqual(0, Invitation.objects.filter(pk=self.invitation.pk).count())

    @TestViewBase.login_as_user1
    def test_user_CAN_decline_invitation_and_that_will_remove_it_ajax(self):
        response = self.client.get(reverse("decline-invitation",
                                           kwargs={"pk": self.invitation.pk}),
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(200, response.status_code)
        data = json.loads(response.content)
        self.assertEqual(data['status'], "success")
        self.assertEqual(0, Invitation.objects.filter(pk=self.invitation.pk).count())

    @TestViewBase.login_as_user2
    def test_from_user_cant_decline_invitation(self):
        response = self.client.get(reverse("decline-invitation",
                                           kwargs={"pk": self.invitation.pk}))
        self.assertEqual(400, response.status_code)
        self.assertEqual(1, Invitation.objects.filter(pk=self.invitation.pk).count())


class TestCancelInvitationView(TestViewBase):
    def setUp(self):
        super(TestCancelInvitationView, self).setUp()
        self.invitation = Invitation.objects.create(
            from_user=self.user2,
            to_user=self.user1
        )

    def test_guest_CANT_cancel_invitation(self):
        response = self.client.get(reverse("cancel-invitation",
                                           kwargs={"pk": self.invitation.pk}))
        self.assertEqual(302, response.status_code)

    @TestViewBase.login_as_user2
    def test_from_user_CAN_cancel_invitation(self):
        response = self.client.get(reverse("cancel-invitation",
                                           kwargs={"pk": self.invitation.pk}))
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, Invitation.objects.filter(pk=self.invitation.pk).count())

    @TestViewBase.login_as_user2
    def test_from_user_CANT_cancel_accepted_invitation(self):
        self.invitation.status = Invitation.STATUS_ACCEPT
        self.invitation.save()

        response = self.client.get(reverse("cancel-invitation",
                                           kwargs={"pk": self.invitation.pk}))
        self.assertEqual(400, response.status_code)
        self.assertEqual(1, Invitation.objects.filter(pk=self.invitation.pk).count())

    @TestViewBase.login_as_user1
    def test_invited_user_CANT_cancel_invitation(self):
        response = self.client.get(reverse("cancel-invitation",
                                           kwargs={"pk": self.invitation.pk}))
        self.assertEqual(400, response.status_code)
        self.assertEqual(1, Invitation.objects.filter(pk=self.invitation.pk).count())


class TestCheckInvitationView(TestViewBase):
    def setUp(self):
        super(TestCheckInvitationView, self).setUp()
        board = Board.objects.create()
        self.invitation = Invitation.objects.create(
            from_user=self.user2,
            to_user=self.user1,
            board=board,
        )

    def test_guest_CANT_check_invitation_status(self):
        response = self.client.get(reverse("check-invitation-status",
                                           kwargs={"pk": self.invitation.pk}))
        self.assertEqual(302, response.status_code)

    @TestViewBase.login_as_user1
    def test_user_CAN_check_invitation_status_ajax(self):
        response = self.client.get(reverse("check-invitation-status",
                                           kwargs={"pk": self.invitation.pk}),
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(200, response.status_code)
        data = json.loads(response.content)
        self.assertIsNone(data["html"])
        self.assertIsNone(data["hash"])
        self.assertEqual(data["board_url"],
                         reverse("board", kwargs={"pk": self.invitation.board_id}))

    @TestViewBase.login_as_user1
    def test_user_CAN_check_invitation_status_with_hash_ajax(self):
        response = self.client.get(
            reverse("check-invitation-status", kwargs={"pk": self.invitation.pk}), {
                "hash": ""
            },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(200, response.status_code)
        data = json.loads(response.content)
        self.assertIn("html", data)
        self.assertIsNotNone(data["hash"])
        self.assertEqual(data["board_url"],
                         reverse("board", kwargs={"pk": self.invitation.board_id}))


class TestStepView(TestViewBase):
    def setUp(self):
        super(TestStepView, self).setUp()
        self.board = Board.objects.create(
            user1=self.user1,
            user2=self.user2,
        )
        self.board.set_state(1, 1, self.user2.pk)
        self.board.save()

    def test_guest_cant_make_step(self):
        response = self.client.get(
            reverse("step", kwargs={"pk": self.board.pk, "x": 0, "y": 0}))
        self.assertEqual(302, response.status_code)

    @TestViewBase.login_as_user1
    def test_user_CAN_make_step_if_his_turn_ajax(self):
        response = self.client.get(
            reverse("step", kwargs={"pk": self.board.pk, "x": 0, "y": 0}),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)

        self.assertIsNotNone(data["board"])
        self.assertIsNone(data["winner"])
        self.assertFalse(data["is_complete"])

        self.board.refresh_from_db()
        self.assertEqual(self.user2.pk, self.board.current_user.pk)
        self.assertEqual(self.user1.pk, self.board.get_state(0, 0))

    @TestViewBase.login_as_user1
    def test_user_CAN_make_step_if_his_turn(self):
        response = self.client.get(
            reverse("step", kwargs={"pk": self.board.pk, "x": 0, "y": 0}),
            HTTP_REFERER=reverse("board", kwargs={"pk": self.board.pk}))
        self.assertRedirects(response, reverse("board", kwargs={"pk": self.board.pk}))

        self.board.refresh_from_db()
        self.assertEqual(self.user2.pk, self.board.current_user.pk)
        self.assertEqual(self.user1.pk, self.board.get_state(0, 0))

    @TestViewBase.login_as_user1
    def test_user_CANT_change_value_of_occupied_spot(self):
        response = self.client.get(
            reverse("step", kwargs={"pk": self.board.pk, "x": 1, "y": 1}))
        self.assertRedirects(response, reverse("board", kwargs={"pk": self.board.pk}))

        self.board.refresh_from_db()
        self.assertEqual(self.user1.pk, self.board.current_user.pk)
        self.assertEqual(self.user2.pk, self.board.get_state(1, 1))

    @TestViewBase.login_as_user2
    def test_user_CANT_make_step_if_not_his_step(self):
        response = self.client.get(
            reverse("step", kwargs={"pk": self.board.pk, "x": 2, "y": 2}))
        self.assertRedirects(response, reverse("board", kwargs={"pk": self.board.pk}))

        self.board.refresh_from_db()
        self.assertEqual(self.user1.pk, self.board.current_user.pk)
        self.assertEqual(0, self.board.get_state(2, 2))

    @TestViewBase.login_as_user2
    def test_user_CANT_make_step_if_not_his_step_ajax(self):
        response = self.client.get(
            reverse("step", kwargs={"pk": self.board.pk, "x": 2, "y": 2}),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 400)

        self.board.refresh_from_db()
        self.assertEqual(0, self.board.get_state(2, 2))

    @TestViewBase.login_as_user1
    def test_user_CAN_became_winner(self):
        self.board.set_state(2, 0, self.user1.pk)
        self.board.set_state(2, 1, self.user1.pk)
        self.board.save()
        response = self.client.get(
            reverse("step", kwargs={"pk": self.board.pk, "x": 2, "y": 2}),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)

        self.assertEqual(data['winner'], self.user1.pk)
