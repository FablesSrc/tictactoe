# coding=utf-8
import json
import datetime

from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.conf import settings


class Board(models.Model):
    width = models.IntegerField(default=3)
    height = models.IntegerField(default=3)
    line_width = models.IntegerField(default=3)
    state = models.TextField(default="")

    current_user = models.ForeignKey(User, related_name='%(class)s_current_user', null=True,
                                     blank=True,
                                     help_text="the user who plays now")

    user1 = models.ForeignKey(User, related_name='%(class)s_user1', null=True, blank=True)
    user2 = models.ForeignKey(User, related_name='%(class)s_user2', null=True, blank=True)
    winner = models.ForeignKey(User, related_name='%(class)s_winner', null=True, blank=True)

    is_complete = models.BooleanField(default=False)
    is_timeout = models.BooleanField(default=False)

    parent_board = models.ForeignKey("self", null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def user_boards(self, user):
        """
        Возвращает все игровые поля где играл пользователь user
        :type user: User
        """
        return Board.objects.filter(Q(user1=user) | Q(user2=user))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.pk is None:
            # fill board
            self.state = json.dumps([0] * (self.width * self.height))
            # set current user
            if self.current_user is None:
                self.current_user = self.user1

        # call default
        super(Board, self).save(force_insert, force_update, using, update_fields)

    def get_matrix_value(self, x, y, state_matrix):
        return state_matrix[x + y * self.width]

    def set_matrix_value(self, x, y, state_matrix, value):
        state_matrix[x + y * self.width] = value

    def set_state(self, x, y, value):
        """
        устанавливает значение value для поля x y
        """
        x = int(x)
        y = int(y)
        state = json.loads(self.state)
        if x < 0 or y < 0 or x + y * self.width > self.width * self.height:
            return False

        if self.get_matrix_value(x, y, state) == 0:
            self.set_matrix_value(x, y, state, value)

        self.state = json.dumps(state)

        self.check_complete()

        return True

    def get_state(self, x, y):
        """
        возвращает состояние поля на доске
        """
        state = json.loads(self.state)
        return self.get_matrix_value(x, y, state)

    def get_winner(self, x=None, y=None, state=None):
        """
        возвращает номер победителя либо None если победителя нет
        """
        if not state:
            state = json.loads(self.state)

        def eq(x, y, value):
            """
            эта функция пороверяет, совпадает ли значение в клетке (x, y) с value
            """
            if x < 0 or y < 0 or x >= self.width or y >= self.width:
                return False
            return self.get_matrix_value(x, y, state) == value

        # из каждой непустой клетки пускает восемь лучей
        # и пытаетcя довести до конца хотя бы один
        for x in xrange(self.width):
            for y in xrange(self.height):
                # смотрим значение в текущей клетке
                base = self.get_matrix_value(x, y, state)
                if base == 0:  # пустые клетки пропускаем
                    continue

                counter = 1  # сбрасываем счетчик
                values = [True] * 8  # сбрасываем массив
                for i in xrange(1, self.line_width):
                    # пускаем не более восьми лучей из текущего поля
                    if values[0]:
                        values[0] = eq(x - i, y, base)
                    if values[1]:
                        values[1] = eq(x + i, y, base)
                    if values[2]:
                        values[2] = eq(x, y - i, base)
                    if values[3]:
                        values[3] = eq(x, y + i, base)
                    if values[4]:
                        values[4] = eq(x + i, y + i, base)
                    if values[5]:
                        values[5] = eq(x + i, y - i, base)
                    if values[6]:
                        values[6] = eq(x - i, y - i, base)
                    if values[7]:
                        values[7] = eq(x - i, y + i, base)

                    # если хотя бы один луч не попал на пустое поле
                    if any(values):
                        counter += 1  # увеличиваем радиус поиска на единицу
                    else:
                        break  # иначе переходим на следующую клетку

                # если для текущей клетки был найден луч необходимой длины
                # возвращаем номер победителя
                if counter == self.line_width:
                    return base
        return None

    def check_time_elapse(self, force_update=False):
        """
        проверяет не вышло ли время у текущего игрока
        в случае положительного ответа завершает игру
        в пользу предыдущего игрока
        """
        if not self.is_complete:
            # если с момента последнего хода прошло времени положеного на один ход,
            # то засчитываем выиграш предыдущему игроку
            if self.time_left <= 0:
                self.winner = self.user1 if self.current_user == self.user2 else self.user2
                self.is_complete = True
                self.is_timeout = True
                if force_update:
                    self.save(update_fields=["winner", "is_complete", "is_timeout"])

    @property
    def time_left(self):
        """
        возвращает количество времени оставшееся до конца текущего хода
        """
        now = datetime.datetime.now(timezone.utc)
        delta = now - self.updated_at
        return max(0, settings.BOARD_STEP_TIMEOUT - delta.total_seconds())

    def check_complete(self, force_update=False):
        """
        возвращает true если игра окончена
        также делает все необходимые проверки
        """
        state = json.loads(self.state)

        if not self.winner:
            winner = self.get_winner()
            if winner:
                self.winner = User.objects.get(pk=winner)

        self.is_complete = True if self.winner or 0 not in state else False

        if force_update:
            Board.objects.filter(pk=self.pk).update(winner=self.winner,
                                                    is_complete=self.is_complete)

        return self.is_complete
