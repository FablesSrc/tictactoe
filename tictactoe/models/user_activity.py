# coding=utf-8
import datetime

from django.contrib.auth.models import User
from django.db import models


class UserActivity(models.Model):
    user = models.ForeignKey(User)
    last_activity_at = models.DateTimeField(auto_now=True)

    @classmethod
    def active_users(cls, request=None):
        """
        возвращает список активных игроков,
        то есть тех кто присутствовал на сайте
        в последную минуту
        """
        close_time = datetime.datetime.now() - datetime.timedelta(minutes=1)
        active_users_pk = UserActivity.objects.filter(last_activity_at__gte=close_time).values("user")
        data = User.objects.filter(pk__in=active_users_pk)
        return data
