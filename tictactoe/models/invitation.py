# coding=utf-8
from django.contrib.auth.models import User
from django.db import models


class Invitation(models.Model):
    STATUS_PENDING = 0
    STATUS_DECLINE = 1
    STATUS_ACCEPT = 2

    STATUS = (
        (STATUS_PENDING, "pending"),
        (STATUS_DECLINE, "decline"),
        (STATUS_ACCEPT, "accept"),
    )

    from_user = models.ForeignKey(User, related_name='%(class)s_from_user')
    to_user = models.ForeignKey(User, related_name='%(class)s_to_user')

    request_json = models.TextField(default="")

    status = models.IntegerField(choices=STATUS, default=STATUS_PENDING)

    board = models.ForeignKey("Board", null=True, blank=True, default=None)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def user_from_invitations(cls, user):
        """
        возвращает список приглашений от пользователя
        :type user: User
        """
        return Invitation.objects.filter(from_user=user)

    @classmethod
    def user_to_invitations(cls, user):
        """
        возвращает список приглашений к пользователю
        :type user: User
        """
        return Invitation.objects.filter(to_user=user)
