from django.template import Library

register = Library()


@register.filter
def range(value):
    return xrange(value)


@register.simple_tag
def get_state(x, y, board, user):
    state = board.get_state(x, y)
    if board.user1 and board.user1.pk == state:
        return "tic fa fa-circle-thin disabled"
    if board.user2 and board.user2.pk == state:
        return "tac fa fa-times disabled"
    return ""


@register.filter
def divide(value, arg):
    return int(value) / int(arg)
