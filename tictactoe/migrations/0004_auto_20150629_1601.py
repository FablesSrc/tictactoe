# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('tictactoe', '0003_invitation'),
    ]

    operations = [
        migrations.AddField(
            model_name='board',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 29, 16, 1, 32, 876392, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='board',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 29, 16, 1, 40, 347927, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invitation',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 29, 16, 1, 53, 388744, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invitation',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'pending'), (1, b'decline'), (2, b'accept')]),
        ),
        migrations.AddField(
            model_name='invitation',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 29, 16, 1, 58, 8535, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='invitation',
            name='board',
            field=models.ForeignKey(default=None, blank=True, to='tictactoe.Board', null=True),
        ),
    ]
