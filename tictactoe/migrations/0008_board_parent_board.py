# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tictactoe', '0007_board_winner'),
    ]

    operations = [
        migrations.AddField(
            model_name='board',
            name='parent_board',
            field=models.ForeignKey(blank=True, to='tictactoe.Board', null=True),
        ),
    ]
