# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tictactoe', '0006_useractivity'),
    ]

    operations = [
        migrations.AddField(
            model_name='board',
            name='winner',
            field=models.ForeignKey(related_name='board_winner', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
