# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Board',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('width', models.IntegerField(default=3)),
                ('height', models.IntegerField(default=3)),
                ('state', models.TextField(default=b'')),
                ('users', models.ManyToManyField(help_text=b'list of users who can play on this board', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
