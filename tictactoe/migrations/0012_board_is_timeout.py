# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tictactoe', '0011_board_is_complete'),
    ]

    operations = [
        migrations.AddField(
            model_name='board',
            name='is_timeout',
            field=models.BooleanField(default=False),
        ),
    ]
