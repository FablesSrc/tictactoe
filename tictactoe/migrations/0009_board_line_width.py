# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tictactoe', '0008_board_parent_board'),
    ]

    operations = [
        migrations.AddField(
            model_name='board',
            name='line_width',
            field=models.IntegerField(default=3),
        ),
    ]
