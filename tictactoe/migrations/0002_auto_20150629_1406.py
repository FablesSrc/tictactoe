# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tictactoe', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='board',
            name='users',
        ),
        migrations.AddField(
            model_name='board',
            name='user1',
            field=models.ForeignKey(related_name='board_user1', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='board',
            name='user2',
            field=models.ForeignKey(related_name='board_user2', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
