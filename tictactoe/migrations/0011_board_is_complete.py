# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tictactoe', '0010_invitation_request_json'),
    ]

    operations = [
        migrations.AddField(
            model_name='board',
            name='is_complete',
            field=models.BooleanField(default=False),
        ),
    ]
