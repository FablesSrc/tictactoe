# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tictactoe', '0004_auto_20150629_1601'),
    ]

    operations = [
        migrations.AddField(
            model_name='board',
            name='current_user',
            field=models.ForeignKey(related_name='board_current_user', blank=True, to=settings.AUTH_USER_MODEL, help_text=b'the user who plays now', null=True),
        ),
    ]
