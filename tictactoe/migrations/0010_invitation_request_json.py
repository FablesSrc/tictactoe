# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tictactoe', '0009_board_line_width'),
    ]

    operations = [
        migrations.AddField(
            model_name='invitation',
            name='request_json',
            field=models.TextField(default=b''),
        ),
    ]
