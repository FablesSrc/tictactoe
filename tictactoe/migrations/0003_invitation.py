# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tictactoe', '0002_auto_20150629_1406'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invitation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('board', models.ForeignKey(to='tictactoe.Board')),
                ('from_user', models.ForeignKey(related_name='invitation_from_user', to=settings.AUTH_USER_MODEL)),
                ('to_user', models.ForeignKey(related_name='invitation_to_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
