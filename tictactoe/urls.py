from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import login_required
from tictactoe.views import *

urlpatterns = patterns("tictactoe.views",
   url(r'im-here/$', login_required(ImHereView.as_view()), name="im-here"),
   url(r'invitation/to_user/(?P<pk>\d+)/$', login_required(InviteUserView.as_view()), name='invite-user'),
   url(r'invitation/(?P<pk>\d+)/check/$', login_required(CheckInvitationStatusView.as_view()), name="check-invitation-status"),
   url(r'invitation/(?P<pk>\d+)/cancel/$', login_required(CancelInvitationView.as_view()), name="cancel-invitation"),
   url(r'invitation/(?P<pk>\d+)/accept/$', login_required(AcceptInvitationView.as_view()), name="accept-invitation"),
   url(r'invitation/(?P<pk>\d+)/decline/$', login_required(DeclineInvitationView.as_view()), name="decline-invitation"),
   url(r'invitations/', login_required(InvitationsHashedListView.as_view()), name="invitations"),
   url(r'is-my-step-on-board/(?P<pk>\d+)/$', login_required(IsMyStepView.as_view()), name="is-my-step"),
   url(r'acitve-users/$', login_required(ActiveUsersListView.as_view()), name="active-users"),
   url(r'has-invitation-from/(?P<pk>\d+)/$', login_required(CheckForRepeatInvitationView.as_view()), name="has-invitation-from"),
   url(r'board/(?P<pk>\d+)/$', login_required(BoardDetailView.as_view()), name="board"),
   url(r'board/(?P<pk>\d+)/step/x/(?P<x>\d+)/y/(?P<y>\d+)$', login_required(StepView.as_view()), name="step"),
   url(r'$', login_required(IndexPageView.as_view()), name="index"),
)