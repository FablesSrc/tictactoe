from datetime import datetime
import time
from django.conf import settings
from django.db.transaction import atomic
from django.http import JsonResponse, HttpResponse, HttpResponseNotModified
from django.utils import dateformat
from django.views.generic.detail import BaseDetailView
import gevent


class HashedViewMixin(object):
    def get_object_function(self):
        raise NotImplementedError()

    def hash(self, object=None):
        raise NotImplementedError()

    def get(self, request, *args, **kwargs):
        hash_index = None
        if "hash" in self.request.GET:
            startime = datetime.now()
            while True:
                current_time = datetime.now()
                delta = current_time - startime

                hash_index = self.hash(self.get_object_function())
                if "hash" in self.request.GET and self.request.GET['hash']:
                    if hash_index == self.request.GET["hash"]:
                        pass
                    else:
                        response = super(HashedViewMixin, self).get(request, *args, **kwargs)
                        break
                else:
                    response = super(HashedViewMixin, self).get(request, *args, **kwargs)
                    break

                if delta.total_seconds() > settings.LONG_POLLING_TIMEOUT:
                    return HttpResponse()

                gevent.sleep(1)
        else:
            response = super(HashedViewMixin, self).get(request, *args, **kwargs)

        if request.is_ajax():
            self.object = self.get_object_function()
            return JsonResponse(self.get_data({
                'html': response.rendered_content if hasattr(response,
                                                             "rendered_content") else None,
                'hash': hash_index,
            }))

        return response

    def get_data(self, context):
        return context


class HashedDetailViewMixin(HashedViewMixin):
    def hash(self, object=None):
        obj = self.get_object()
        if obj:
            updated_at = obj.updated_at
            timestamp = dateformat.format(updated_at, "U") if updated_at else ""
            return "%s" % timestamp
        return ""

    def get_object_function(self):
        return self.get_object()


class HashedListViewMixin(HashedViewMixin):
    """
    returns nothing if user provide hash and returned query_set has the same hash
    hash function must be reimplemented
    """

    def hash(self, query_set):
        raise NotImplementedError()

    def get_object_function(self):
        return self.get_queryset()
