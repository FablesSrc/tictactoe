# coding=utf-8

from django.core.urlresolvers import reverse
from django.http.response import HttpResponse
from django.views.generic.detail import BaseDetailView

from tictactoe.views._hashed_views import HashedDetailViewMixin
from tictactoe.models import Invitation


class CheckInvitationStatusView(HashedDetailViewMixin, BaseDetailView):
    """
    Запускает longpoll цикл проверки статус принятия приглашения
    Если приглашение приняли возвращет ссылку на соответсвующее поле с игрой
    """
    model = Invitation

    def get_data(self, context):
        invitation = self.get_object()
        if invitation.board:
            context.update({
                "board_url": reverse("board", kwargs={"pk": invitation.board_id})
            })
        return context

    def render_to_response(self, context, **response_kwargs):
        return HttpResponse()
