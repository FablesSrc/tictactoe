# coding=utf-8

from django.contrib.auth.models import User
from django.views.generic.list import ListView

from tictactoe.views._hashed_views import HashedListViewMixin
from tictactoe.models import UserActivity


class ActiveUsersListView(HashedListViewMixin, ListView):
    """
    в случае передачи параметра hash
    запускает longpoll цикл анализа данных на сервере
    на предмет изменения списка активных пользователей
    """
    model = User
    template_name = "tictactoe/_active_users.html"
    user_list = None

    def hash(self, user_list):
        return "-".join(["{id}{username}".format(**user) for user in user_list.order_by("id").values("id", "username")])

    def get_queryset(self):
        return UserActivity.active_users(self.request)
