# coding=utf-8

from django.core.urlresolvers import reverse
from django.db.transaction import atomic
from django.http.response import HttpResponseBadRequest, JsonResponse, Http404
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.views.generic.base import View

from tictactoe.models import Board


class StepView(View):
    """
    выполнеяет один шаг для текущего пользовтаеля и меняет активного игрока на доске
    """
    @atomic
    def get(self, request, *args, **kwargs):
        # выбираем доску
        try:
            board = Board.objects.select_for_update().get(pk=self.kwargs['pk'])
        except Board.DoesNotExist:
            raise Http404('No %s matches the given query.' % Board.object_name)

        # определяе номер позиции на клетке
        x = int(self.kwargs["x"])
        y = int(self.kwargs["y"])

        # проверяем не вышло ли игровое время
        board.check_time_elapse(True)

        # если наш ход и клетка не занята
        if not board.is_complete and board.current_user == request.user and board.get_state(x, y) == 0:
            # если шаг был успешен
            if board.set_state(x, y, request.user.pk):
                board.current_user = board.user2 if request.user == board.user1 else board.user1
                board.save()

            if request.is_ajax():
                return JsonResponse({
                    "board": render_to_string("tictactoe/_board.html", {
                        "board": board
                    }),
                    "winner": board.winner_id,
                    "is_complete": board.is_complete,
                })
        else:
            if request.is_ajax():
                return HttpResponseBadRequest()

        if 'HTTP_REFERER' in request.META:
            return redirect(request.META['HTTP_REFERER'])

        return redirect(reverse("board", kwargs={"pk": board.pk}))
