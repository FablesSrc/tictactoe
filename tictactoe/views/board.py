# coding=utf-8

from django.db.transaction import atomic
from django.views.generic.detail import DetailView

from tictactoe.models import Board, Invitation


class BoardDetailView(DetailView):
    """
    возврвщает страницу с игровым полем
    """
    model = Board
    template_name = "tictactoe/board.html"

    @atomic
    def get_context_data(self, **kwargs):
        context = super(BoardDetailView, self).get_context_data(**kwargs)
        if not self.object.is_complete:
            self.object.check_time_elapse(True)
        context['invitation'] = Invitation.objects.filter(board=self.object).first()
        return context
