# coding=utf-8

from django.http.response import HttpResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from django.views.generic.base import View

from tictactoe.models import Invitation


class CancelInvitationView(View):
    """
    Отмена соглашения с номером pk,
    при отмене объект приглашения удаляется
    """

    def get(self, request, *args, **kwargs):
        invitation = get_object_or_404(Invitation, pk=self.kwargs["pk"])

        # контролируем что это наше соглашение
        if invitation.from_user != request.user:
            return HttpResponseBadRequest("You can't cancel not yours invitation")

        # если приглашение кто-то принял его нельзя отменитт
        if invitation.status == Invitation.STATUS_ACCEPT:
            return HttpResponseBadRequest(
                "can't delete invitation, `%s` accepts it" % invitation.to_user.username)

        # иначе отменям приглашение
        if invitation.status in [Invitation.STATUS_PENDING, Invitation.STATUS_DECLINE]:
            invitation.delete()
            return HttpResponse("successfully delete invitation")
