# coding=utf-8
import json

from django.core.urlresolvers import reverse
from django.http.response import JsonResponse
from django.shortcuts import redirect, get_object_or_404
from django.views.generic.base import View

from tictactoe.models import Board, Invitation


class AcceptInvitationView(View):
    """
    потдверждает приглашение с pk
    """
    def get(self, request, *args, **kwargs):
        invitation = get_object_or_404(Invitation, pk=self.kwargs['pk'])
        invitation.status = Invitation.STATUS_ACCEPT

        # дефолтные размеры поля
        width = 3
        height = 3
        line_width = 3
        # если при приглашении передавались
        # какие-то спецефичиские параметры
        # используем их
        if invitation.request_json:
            data = json.loads(invitation.request_json)
            if "big" in data:
                line_width = 5
                height = 10
                width = 10
            else:
                if "width" in data:
                    width = int(data['width'])
                if "height" in data:
                    height = int(data['height'])
                if "line_width" in data:
                    line_width = int(data['line_width'])

        #  создаем игровое поле
        board = Board.objects.create(
            width=width,
            height=height,
            line_width=line_width,
            user1=invitation.from_user,
            user2=invitation.to_user,
            current_user=invitation.from_user,
        )

        # сохраняем приглашение
        invitation.board = board
        invitation.save()

        if request.is_ajax():
            return JsonResponse({
                "board": board.pk
            })
        else:
            return redirect(reverse("board", kwargs={"pk": board.pk}))
