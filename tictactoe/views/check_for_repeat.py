# coding=utf-8

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse
from django.views.generic.detail import BaseDetailView

from tictactoe.views._hashed_views import HashedDetailViewMixin
from tictactoe.models import Invitation


class CheckForRepeatInvitationView(HashedDetailViewMixin, BaseDetailView):
    """
    Запускает longpoll цикл проверки
    на наличие повторного приглашения от пользователя с номером pk
    """
    model = User

    def hash(self, object=None):
        # хэш функция, если повторное приглашение есть хэш равен 1
        return "0" if object is None else "1"

    def get_object(self, queryset=None):
        # ищем повторное приглашение
        self.user = super(CheckForRepeatInvitationView, self).get_object(queryset)
        return Invitation.objects.filter(from_user=self.user, to_user=self.request.user,
                                         status=Invitation.STATUS_PENDING).first()

    def render_to_response(self, context):
        return HttpResponse()

    def get_data(self, context):
        if self.object:
            # если повторное приглашение есть то в ответ добавляем ссылку на принятие и отмену приглашения
            context.update({
                "accept": reverse("accept-invitation", kwargs={"pk": self.object.pk}),
                "decline": reverse("decline-invitation", kwargs={"pk": self.object.pk}),
            })
        return context
