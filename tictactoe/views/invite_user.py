# coding=utf-8
import json

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.base import View

from tictactoe.models import Invitation


class InviteUserView(View):
    """
    Создает приглашение от текщего пользоватлея к пользователю с pk
    возвращает json с / ссылкой на отмену / ссылкой на проверку / номером нового приглашения /
    """
    def get(self, request, *args, **kwargs):
        user = get_object_or_404(User, pk=self.kwargs['pk'])

        # себя не приглашаем
        if user == request.user:
            return HttpResponseBadRequest("User can't invite himself!")

        # нельзя создать более одного приглашения
        if Invitation.objects.filter(from_user=request.user,
                                     status=Invitation.STATUS_PENDING).count() > 0:
            return HttpResponseBadRequest("You can create more then one invitation at a time")

        invitation = Invitation.objects.create(
            from_user=request.user,
            to_user=user,
            request_json=json.dumps(request.GET),
            board=None,
            status=Invitation.STATUS_PENDING
        )

        if request.is_ajax():
            return JsonResponse({
                "cancel": reverse("cancel-invitation",
                                  kwargs={"pk": invitation.pk}),
                "check": reverse("check-invitation-status",
                                 kwargs={"pk": invitation.pk}),
                "invitation": invitation.pk,
            })
        return redirect(reverse("index"))
