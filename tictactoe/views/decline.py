# coding=utf-8

from django.core.urlresolvers import reverse
from django.db.transaction import atomic
from django.http.response import HttpResponseBadRequest, JsonResponse
from django.shortcuts import redirect, get_object_or_404
from django.views.generic.base import View

from tictactoe.models import Invitation


class DeclineInvitationView(View):
    """
    Отмена приглашения которого отправили нам
    """
    @atomic
    def get(self, request, *args, **kwargs):
        invitation = get_object_or_404(Invitation, pk=self.kwargs['pk'])

        # мы можем только отменить только свое приглашение
        if request.user == invitation.to_user:
            invitation.delete()
            if self.request.is_ajax():
                return JsonResponse({
                    "status": "success",
                })
            return redirect(reverse("index"))
        else:
            return HttpResponseBadRequest("Only invited user can decline invitation")
