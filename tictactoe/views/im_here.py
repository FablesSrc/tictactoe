# coding=utf-8

from django.db.transaction import atomic
from django.http.response import HttpResponse
from django.views.generic.base import View

from tictactoe.models import UserActivity


class ImHereView(View):
    """
    Обновление даты последнего посещения приложения текущего пользователя
    """
    @atomic
    def get(self, request, *args, **kwargs):
        user_activity, _ = UserActivity.objects.get_or_create(user=self.request.user)
        user_activity.save()
        return HttpResponse()
