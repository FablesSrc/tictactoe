# coding=utf-8

from django.db.models import Max
from django.views.generic.list import ListView
import django.utils.dateformat as dateformat

from tictactoe.views._hashed_views import HashedListViewMixin
from tictactoe.models import Invitation


class InvitationsHashedListView(HashedListViewMixin, ListView):
    """
    в случае передачи параметра hash
    запускает longpoll цикл анализа данных на сервере
    на предмет изменения списка активных приглашений к текущему пользователю
    """
    model = Invitation
    template_name = "tictactoe/_invitations.html"

    def hash(self, query_set):
        updated_at = query_set.aggregate(Max("updated_at")).values()[0]
        timestamp = dateformat.format(updated_at, "U") if updated_at else ""
        return "%s%s" % (self.request.user.username, timestamp)

    def get_queryset(self):
        invitations = Invitation.objects.filter(to_user=self.request.user, board=None)
        return invitations
