# coding=utf-8

from django.views.generic.detail import DetailView

from tictactoe.views._hashed_views import HashedDetailViewMixin
from tictactoe.models import Board


class IsMyStepView(HashedDetailViewMixin, DetailView):
    """
    в случае предачи параметра hash в запросе
    запускает longpoll цикл провеки 
    изменения статуса текущего хода на игровой доске
    """
    model = Board
    template_name = "tictactoe/_board.html"

    def get_object_function(self):
        obj = super(IsMyStepView, self).get_object_function()
        if not obj.is_complete:
            obj.check_time_elapse(True)
            obj.refresh_from_db()

        return obj

    def get_data(self, context):
        context.update({
            "winner": self.object.winner_id,
            "is_complete": self.object.is_complete,
            "my_step": self.object.current_user == self.request.user,
            "time_left": self.object.time_left
        })
        return context