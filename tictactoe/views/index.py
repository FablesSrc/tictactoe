# coding=utf-8

from django.db.models import Q
from django.views.generic.base import TemplateView

from tictactoe.models import Board, Invitation, UserActivity


class IndexPageView(TemplateView):
    """
    главная страница
    """
    template_name = "tictactoe/index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexPageView, self).get_context_data(**kwargs)
        context.update({
            "my_boards": Board.user_boards(self.request.user).order_by("-created_at")[:20],
            "invitations": Invitation.user_to_invitations(self.request.user).filter(board=None),
            "my_invitations": Invitation.user_from_invitations(self.request.user).filter(status=Invitation.STATUS_PENDING),
            "active_users": UserActivity.active_users(self.request)
        })
        return context
