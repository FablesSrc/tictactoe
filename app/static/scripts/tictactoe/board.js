/***
 * Контроллер игровой доски
 * @param myTurn {boolean} мой ли сейчас ход
 * @param winner {int} кто победитель игры
 * @param is_complete {boolean} закончилась ли игра
 * @param time_left {float} времени осталось до конца текущего хода
 * @param urls {object} основные ссылки
 * @constructor
 */
function BoardControl(myTurn, winner, is_complete, time_left, urls) {
	var self = this;
	var counterControl = new CounterControl(self);
	var my_step_hash = null;

	self.myTurn = myTurn;
	self.winner = winner;
	self.is_complete = is_complete;
	self.time_left = time_left;

	/***
	 * Проверяет соответствует ли текцщий ответ сервера окончанияю игры
	 * @param response ответ с сервера на ajax запрос
	 * @returns {boolean}
	 */
	function checkComplete(response) {
		if (response && (response.winner || response.is_complete)) { // если игра закончилась, перегружаем страницу
			window.location = urls.item;
			return true;
		}
		return false;
	}


	/***
	 *
	 * @param response
	 */
	function setMyStep(response) {
		// удачный ответ означает что сейчас наш ход
		if (checkComplete(response))
			return;

		if (response) {
			$("#board-field").html(response.html);
		}
	}

	/***
	 * Обновление интерфейса доски
	 * @param response
	 */
	function updateInterface(response) {
		if (response) {
			my_step_hash = response.hash;
			self.myTurn = response.my_step;
			if (self.myTurn) {
				$("#wait-screen").slideUp();
				counterControl.show();
			} else {
				$("#wait-screen").slideDown();
				counterControl.hide();
			}
			counterControl.resetCounter(response.time_left);
		}

	}

	/***
	 * Отправляет запрос на сервер, который отвечает при изменении текущего игрока
	 */
	function checkMyStep() {
		// отправку запроса на сервер имеет смысл делать только,
		// если в игре нет победителя, сейчас не ход игрока и если игра не закончена
		if (!self.winner && !self.myTurn && !self.is_complete) {
			$.get(urls.is_my_step, {
				hash: my_step_hash
			}).done(setMyStep).always(function (r) {
				updateInterface(r);
				setTimeout(checkMyStep, 1000);
			});
		}
	}

	/***
	 * реакция на клик по доске,
	 * обновляет
	 * реагируем только если наш ход
	 */
	function fieldClick() {
		if (self.myTurn) { // если наш ход
			$.get(this.href).done(function (r) {
				if (checkComplete(r))
					return;

				// удачный ответ означает что сечас уже не наш ход
				self.myTurn = false;
				$("#wait-screen").slideDown();
				$("#board-field").html(r.board);

				// запускаем цикл ожидания нашего хода
				checkMyStep();
			});
		}
		return false;
	}

	// реакция на клик по клетке доски
	$(document).on("click", "#board-field .item", fieldClick);

	counterControl.resetCounter(self.time_left);
	counterControl.runCounter(self.is_complete);
	checkMyStep();
}

/***
 * Контролер счетчика обратного отсчета
 * @param boardControl
 * @constructor
 */
function CounterControl(boardControl) {
	var self = this;
	var counter_time_left;

	self.resetCounter = function (time_left) {
		counter_time_left = ~~time_left;
	};

	/***
	 * скрывает счетчик
	 */
	self.hide = function () {
		$("#counter").fadeOut("fast");
	};

	/***
	 * отображает счетчик
	 */
	self.show = function () {
		$("#counter").fadeIn("fast");
	};

	/***
	 * запускает счетчик
	 */
	self.runCounter = function () {
		if (!boardControl.is_complete) {
			if (counter_time_left > 0) {
				$(".counter-value").html(counter_time_left);
				counter_time_left--;
				setTimeout(self.runCounter, 1000)
			} else {
				window.location.reload();
			}
		}
	};
}

/***
 * Контролер конца игры для пользователя, котороый инициировал начало игры
 * @param index_url ссылка на главную страницу
 * @constructor
 */
function CompleteFromUserControl(index_url) {
	var invitation = {};

	/** последний хэш статуса отправленого приглашения */
	var check_invitation_hash = null;

	/***
	 * обновляет интерфейс в соотвтствии с ответом с сервера
	 * @param response
	 */
	function updateInterface(response) {
		invitation = response;
		$("#btn-repeat").remove();
		$("#repeat-loader").fadeIn();
		check_my_invitation();
	}

	/***
	 * реакция на нажатие кнопки повторить
	 */
	function inviteUser() {
		$.get(this.href).done(updateInterface);
		return false;
	}

	/***
	 * запускает цикл проверки статуса отправленого приглашения
	 */
	function check_my_invitation() {
		$.get(invitation.check, {
			hash: check_invitation_hash
		}).done(function (r) {
			if (r) {
				if (check_invitation_hash && r.hash != check_invitation_hash) {
					window.location = r.board_url;
				}
				check_invitation_hash = r.hash;
			}
			setTimeout(check_my_invitation, 1000);
		}).fail(function () {
			window.location = index_url;
		});
	}

	$("#btn-repeat").click(inviteUser);
}

/***
 * Контролер конца игры для пользователя, котороый был приглашен на текущую игру
 * @param has_invitation_from_url сслыка для проверки наличия повторного приглашения
 * от пользователя инициировавашего начало игры
 * @constructor
 */
function CompleteToUserControl(has_invitation_from_url) {
	/** последний хеш статуса повтороного приглашения */
	var invitations_hash = null;

	/***
	 * обновлет интерфейс пользователя в соответствии с ответом о статусе текущего приглашения
	 * @param response
	 */
	function updateInterface(response) {
		if (response) {
			if (response.hash != invitations_hash && response.accept) {
				$('#repeatModal').modal('show');
				$("#btn-accept-repeat").get(0).href = response.accept;
				$("#btn-decline-repeat").get(0).href = response.decline;
			}
			invitations_hash = response.hash;
		}
	}

	/***
	 * запускает цикл проверки наличия повтороного приглашения на текущую игру
	 */
	function has_invitation_from() {
		$.get(has_invitation_from_url, {
			"hash": invitations_hash
		}).done(updateInterface).always(function () {
			setTimeout(has_invitation_from, 1000);
		});
	}

	has_invitation_from();
}