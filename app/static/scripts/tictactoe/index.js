$.ajaxSetup({
	timeout: 40000 // in milliseconds
});

/***
 * проверяет наличие элементов приглашений в DOM дерева
 * @returns {boolean} true если элементы есть, false иначе
 */
function hasYourInvites() {
	return $("#yours-invitation").find("li").size() > 0;
}


/***
 * контроль активных пользователей
 */
function ActiveUsersControl(fetch_active_users_url) {

	/** последний принятый хеш списка активных пользователей */
	var active_users_hash = null;

	/***
	 * проверяет наличие элементов пользователей в DOM дереве
	 * @returns {boolean} true если элементы есть, false иначе
	 */
	function hasActiveUsers() {
		return $("#active-users .content").find("li").size() > 0;
	}

	/***
	 * обновляет список элементов пользоватлей в DOM дереве
	 * @param {object} data ответ выданный сервером на ajax-запрос активных пользоватлей
	 */
	function updateActiveUsers(data) {
		if (data) {
			$("#active-users .content").html(data.html);
			active_users_hash = data.hash;

			if (!hasYourInvites() && hasActiveUsers()) {
				$("#active-users").slideDown();
			} else {
				$("#active-users").slideUp();
			}

			if (!hasActiveUsers()) {
				$("#nobody-status").slideDown();
			} else {
				$("#nobody-status").slideUp();
			}
		}
	}

	/***
	 * запускает цикл pull-push списка активных пользователей с сервера
	 */
	function fetch_active_users() {
		$.get(fetch_active_users_url, {
			"hash": active_users_hash
		}).done(updateActiveUsers).always(function () {
			setTimeout(fetch_active_users, 1000);
		});
	}

	// запускаем цикл pull-push списка активных пользователей с сервера
	fetch_active_users();
}

/***
 * контроль за приглашениями
 * @constructor
 */
function ActiveInvitationControl(fetch_active_invitations_url) {
	/** последний хеш списка приглашений стянутый с сервера */
	var invitations_hash = null;

	/***
	 * проверяет наличие элементов приглашений в DOM дереве
	 * @returns {boolean} true если элементы есть, false иначе
	 */
	function hasInvitations() {
		return $("#invitations .content").find("li").size() > 0;
	}

	/***
	 * отвергнуть приглашение, реакция на нажатие ссылки, в this хранится элемент a,
	 * в this.href -- адрес запроса на отмену приглашения
	 */
	function declineInvitation() {
		var that = this;
		$.get(this.href).done(function (r) {
			$(that).parents(".list-group-item").remove();
		});
		return false;
	}

	/***
	 * обновляет список элементов приглашений в DOM дереве
	 * @param data {object} ответ выданный сервером на ajax-запрос активных приглашений
	 */
	function updateActiveInvitations(data) {
		if (data) {
			$("#invitations .content").html(data.html);
			invitations_hash = data.hash;
			if (hasInvitations()) {
				$("#invitations").slideDown();
			} else {
				$("#invitations").slideUp();
			}
		}
	}

	/***
	 * запуск цикла pull-push активных приглашений
	 */
	function fetch_active_invitations() {
		$.get(fetch_active_invitations_url, {
			"hash": invitations_hash
		}).done(updateActiveInvitations).always(function () {
			setTimeout(fetch_active_invitations(), 1000);
		});
	}

	// подключаем реакцию на нажатие ссылки по отмене приглашения
	$(document).on("click", "#invitations a.ajax", declineInvitation);

	// запускаем цикл pull-push активных приглашений
	fetch_active_invitations();
}

/***
 * контроль моих приглашений
 */
function MyInvitationControl() {
	/** словарь хэшев моих приглашений */
	var check_invitation_hashes = {};

	/***
	 * скрывает список моих приглашений, показывает список активных пользователей
	 */
	function hideMyInvitationsElement() {
		$("#active-users").slideDown();
		$("#yours-invitation").slideUp();
	}

	/***
	 * добавляет элемент моего прглашения в DOM дерево
	 * @param data {object} словарь где
	 * data.check: ссылка на проверку статуса моего приглашений,
	 * data.cancel: ссылка на отмену моего приглашения
	 * @param username {string} имя приглашенного пользователя
	 */
	function addMyInvitationElement(data, username) {
		// прячим список активных пользователей
		$("#active-users").slideUp();

		// генерируем новое элемент приглашения
		var $invite = $("#invitation-template").children().clone();
		$invite.data("check", data.check);
		$invite.find("a").attr("href", data.cancel);
		$invite.find("span.username").html(username);

		// добавляем к списку приглашений (по идее их доложно быть ноль)
		var $element = $("#yours-invitation");
		$element.find("ul").append($invite);
		$element.slideDown();

		checkMyInvitation(null, $invite);
	}

	/***
	 * удаляет мое приглашение связанное с элементов element
	 * @param element элемент моего приглашения в DOM дереве,
	 * либо li, либо a находящийся в li
	 */
	function removeMyInvitationElement(element) {
		check_invitation_hashes[element] = null;

		if ($(element).prop("tagName") != "LI") {
			$(element).parents("li").remove();
		} else {
			$(element).remove();
		}

		if (!hasYourInvites()) {
			hideMyInvitationsElement();
		}
	}

	/***
	 * Создает новое приглашение. В this находится элемент вида a,
	 * такой что a.href -- сслылка на запрос приглашения пользователя
	 * a['data-username'] -- имя пользователя
	 * @returns {boolean}
	 */
	function createMyInvitation() {
		// если есть активные приглашения то надо прежде всего от них избавиться
		if ($("#yours-invitation").find("li").size() > 0)
			return false;

		var username = $(this).data("username");
		$.get(this.href).done(function (r) {
			addMyInvitationElement(r, username)
		});
		return false;
	}

	/***
	 * отмена моего приглашения привязанного к объекту this
	 * В момент вызова в this находится элемент гиперссылка,
	 * у которого href -- ссылка на отмену моего приглашения
	 */
	function cancelMyInvitation() {
		var that = this;
		$.get(that.href).done(function () {
			removeMyInvitationElement(that);
		});
		return false
	}

	/***
	 * Обновляет статус моего приглашения. Как правил возможно только два исхода,
	 * если с сервера пришел ответ и хеш приглашения обновился то запускает новую игру
	 * в остальных случаях просто запускает повторную проверку
	 * @param data ответ с сервера на ajax-запрос
	 * @param item элемент моего приглашения в DOM дереве
	 */
	function updateMyInvitation(data, item) {
		if (data) {
			if (check_invitation_hashes[item] && data.hash != check_invitation_hashes[item]) {
				window.location = data.board_url;
			} else {
				check_invitation_hashes[item] = data.hash;
				//setTimeout(checkMyInvitations, 1000);
				checkMyInvitation(null, item);
			}
		} else {
			checkMyInvitation(null, item);
		}
	}

	/***
	 * Проверка статуса моего приглашения,
	 * в случае если запрос возвращает аварийный код, удаляет приглашение из DOM дерева
	 * @param index порядковый номер приглашения в DOM дереве
	 * @param item элемент моего приглашения в DOM дереве
	 */
	function checkMyInvitation(index, item) {
		var check = $(item).data("check");
		if (check) {
			$.get(check, {
				hash: check_invitation_hashes[item]
			}).done(function (r) {
				updateMyInvitation(r, item);
			}).fail(function () {
				removeMyInvitationElement(item);
			})
		}
	}

	// приглашение пользователя
	$(document).on("click", "#active-users a.invite", createMyInvitation);
	// отмена нашего приглашения, теоретически здесь могут быть старые приглашения
	$(document).on("click", "#yours-invitation a.cancel", cancelMyInvitation);

	// запускаем первичную проверку статуса всех приглашений
	$("#yours-invitation").find("li").each(checkMyInvitation);
}


