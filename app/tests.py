from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import translation


class TestRegisterView(TestCase):
    def test_that_anyone_can_register_GET(self):
        response = self.client.get(reverse("register"))
        self.assertEqual(response.status_code, 200)

    def test_that_anyone_can_register_POST(self):
        response = self.client.post(reverse("register"), {
            "username": "newuser",
            "password1": "123",
            "password2": "123",
        })
        self.assertRedirects(response, reverse("index"))
        self.assertEqual(1, User.objects.filter(username="newuser").count())

        self.assertTrue(self.client.login(username="newuser", password="123"))
        self.client.logout()
        self.assertFalse(self.client.login(username="newuser", password="12"))

    def test_wrong_register_redirects_to_register_page_POST(self):
        response = self.client.post(reverse("register"), {
            "username": "newuser",
            "password1": "123",
            "password2": "1234",
        })
        self.assertEqual(200, response.status_code)


class TestLoginView(TestCase):
    def test_that_anyone_can_access_login_page(self):
        response = self.client.get(reverse("login"))
        self.assertEqual(response.status_code, 200)

    def test_that_anyone_can_login_with_right_credentials(self):
        user = User.objects.create_user(username="user", password="123")

        response = self.client.post(reverse("login"), {
            "username": "user",
            "password": "123",
        })
        self.assertEqual(int(self.client.session['_auth_user_id']), user.pk)
        self.assertRedirects(response, reverse("index"))

    def test_that_anyone_can_login_with_wrong_credentials(self):
        user = User.objects.create_user(username="user", password="123")

        response = self.client.post(reverse("login"), {
            "username": "user",
            "password": "1234",
        })
        self.assertNotIn("_auth_user_id", self.client.session)
        self.assertEqual(response.status_code, 200)


class TestLogoutView(TestCase):
    def test_that_guest_CANT_logout(self):
        response = self.client.get(reverse("logout"))
        self.assertEqual(response.status_code, 302)

    def test_that_user_CAN_logout(self):
        user = User.objects.create_user(username="user", password="123")
        self.client.login(username="user", password="123")
        self.assertEqual(int(self.client.session['_auth_user_id']), user.pk)
        response = self.client.get(reverse("logout"))
        self.assertEqual(response.status_code, 302)
        self.assertNotIn("_auth_user_id", self.client.session)


class TestSetLanguageView(TestCase):
    def test_anyone_can_set_language(self):
        response = self.client.post(reverse("set-language"), {
            "language": "ru"
        }, HTTP_REFERER=reverse("login"))
        self.assertRedirects(response, reverse("login"))
        self.assertEqual(self.client.session[translation.LANGUAGE_SESSION_KEY], "ru")

    def test_user_can_set_language(self):
        user = User.objects.create_user(username="user", password="123")

        self.client.login(username="user", password="123")
        response = self.client.post(reverse("set-language"), {
            "language": "ru"
        }, HTTP_REFERER=reverse("index"))
        self.assertRedirects(response, reverse("index"))
        self.assertEqual(self.client.session[translation.LANGUAGE_SESSION_KEY], "ru")
        self.client.logout()
