from django.conf import settings
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render_to_response, render, redirect
from django.utils import translation
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from tictactoe.models import UserActivity


@sensitive_post_parameters()
@csrf_protect
@never_cache
def register(request):
    if request.method == "POST":
        form_register = UserCreationForm(request.POST)
        if form_register.is_valid():
            user = form_register.save()
            user = authenticate(username=user.username, password=form_register.data['password1'])
            login(request, user)
            return redirect(reverse("index"))
        else:
            form_login = AuthenticationForm()
            return render(request, 'login.html', {
                'form_login': form_login,
                'form_register': form_register,
            })
    elif request.method == "GET":
        form_login = AuthenticationForm()
        form_register = UserCreationForm()
        return render(request, 'login.html', {
            'form_login': form_login,
            'form_register': form_register,
        })
    return HttpResponseBadRequest()


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login_user(request):
    if request.method == "GET":
        form_login = AuthenticationForm()
        form_register = UserCreationForm()
        return render(request, 'login.html', {
            'form_login': form_login,
            'form_register': form_register,
        })
    elif request.method == "POST":
        form_login = AuthenticationForm(request, data=request.POST)
        if form_login.is_valid():
            # Okay, security check complete. Log the user in.
            login(request, form_login.get_user())
            return redirect(reverse("index"))
        else:
            form_register = UserCreationForm()
            return render(request, 'login.html', {
                'form_login': form_login,
                'form_register': form_register,
            })
    return HttpResponseBadRequest()


@login_required
def logout_user(request):
    UserActivity.objects.filter(user=request.user).delete()
    logout(request)
    return redirect(reverse("index"))


def set_language(request):
    if "HTTP_REFERER" in request.META:
        response = redirect(request.META['HTTP_REFERER'])
    else:
        response = redirect(reverse("index"))

    ln = request.POST['language']
    if ln in ['ru', 'en-us', 'en']:
        translation.activate(ln)
        request.session[translation.LANGUAGE_SESSION_KEY] = ln
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, ln)
    return response
