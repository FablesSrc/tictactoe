from django.conf.urls import patterns, include, url
# from django.contrib import admin

urlpatterns = patterns('',
    url(r'^accounts/register/$', 'app.views.register', name="register"),
    url(r'^accounts/login/$', 'app.views.login_user', name="login"),
    url(r'^accounts/logout/$', 'app.views.logout_user', name="logout"),
    url(r'^set-language/$', "app.views.set_language", name="set-language"),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^', include('tictactoe.urls'))
)
